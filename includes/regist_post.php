<?php
if(isset($_POST['id']) or isset($_POST['mdp']) or isset($_POST['mdp2']) or isset($_POST['email']) and $_POST['mdp'] == $_POST['mdp2']) {
date_default_timezone_set('Europe/Paris');
$date = date('y\-m\-d H:i:s');
$password_hache = sha1($_POST['mdp']);
$bdd->query('SET NAMES UTF8');
$req = $bdd->prepare('INSERT INTO userminetest (pseudo, password, email, date) VALUES(:pseudo, :password, :email, :date)');
$req->bindValue(':pseudo', $_POST['id'], PDO::PARAM_STR);
$req->bindValue(':password', $password_hache, PDO::PARAM_STR);
$req->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
$req->bindValue(':date', $date, PDO::PARAM_STR);
$req->execute();
$req->CloseCursor();
header('Location:../index.php');
}
else {
	echo 'Il y a des cases vide !/Les mots de passe ne coresponds pas !';
}
?>
