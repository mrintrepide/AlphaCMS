<?php
	include('code.php');
	include('config.php');
	$n=$_GET['n'];
	if(is_numeric($n)) {
		$bdd->query('SET NAMES UTF8');
		$req = $bdd->prepare('SELECT id, titre, contenu, date_creation FROM newsminetest WHERE id = :id');
		$req->execute(array('id' => $n));
		while ($donnees = $req->fetch()) {
?>
<div class="news_tab">
<?php
			echo '<h2><a href="index.php?p=post&n='.htmlspecialchars($donnees['id']).'">'.htmlspecialchars($donnees['titre']).'</a></h2>';;
			echo code(htmlspecialchars($donnees['contenu']));
			if($_SESSION['level']>=9) {
				echo '<a class="date" href="index.php?p=edit&n='.htmlspecialchars($donnees['id']).'">Edition</a>';
			}
			echo '</div>';
		}
		$req->closeCursor();
		if (isset($_SESSION['id']) AND isset($_SESSION['pseudo'])) {
?>
	<div class="news_tab">
		<form id=form name=form action="includes/com_post.php?n=<?php echo $n; ?>&sid=<?php echo $_SESSION['sid'] ?>" method="post">
			<table>
				<tr>
					<td>
						<textarea rows="10" cols="98" id="contenu" name="contenu"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="Poster" />
					</td>
				</tr>
			</table>
		</form>
<?php
		}
		else {
?>
		<div class="news_tab">
			<p>Connecter vous pour écrire un commentaire.</p>
<?php
		}
		$bdd->query('SET NAMES UTF8');
		$req = $bdd->prepare('SELECT contenu, signature, date_creation FROM comminetest WHERE idpost = :idpost');
		$req->execute(array('idpost' => $n));
		while ($donnees = $req->fetch()) {
?>
			<div class="com">
				<em class="pseudo">
<?php
			echo htmlspecialchars($donnees['signature']);
?>
				</em>
				<em class="date">
<?php
			echo htmlspecialchars($donnees['date_creation']);
?>
				</em>
				</br>
				<p>
<?php
			echo htmlspecialchars($donnees['contenu']);
?>
				</p>
			</div>
<?php
		}
		echo '</div>';
		$req->closeCursor();
	}
?>