<!DOCTYPE html>
<html>
<head>
	<meta name="keywords" content="minecraft,minetest,minetest-fr,wiki,pack,texture,packs,textures,mod,mods,forum,wikipedia,c++,france,fr,clone,lua,français,minetest.fr,server,serveur,jeu,info,information,map,monde,minetest.com,steinheim" />
	<meta name="author" content="hmsintrepide" />
	<meta name="description" content="Minetest-fr est le premier site communautaire français du jeu minetest-c55 (un clone de minecraft en C++ open-source). Le site dispose d'un forum et d'un wiki français. Le serveur soutenu par le site est Steinheim." />
	<meta charset="utf-8" />
	<meta name="google-site-verification" content="0EMmgy0vXKswtztxudnFl-KOiKNIdS7Ul6BK3-Trgac" />
	<meta name="msvalidate.01" content="3535F96E14DC9904E9172AC2A858FCA0" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="icon" href="favicon.ico" />
	<?php
		if($_GET['r']=='post') {
	?>
	<script>
	function bbcode(bbdebut, bbfin) {
		var input = window.document.form.contenu;
		input.focus();
		if(typeof document.selection != 'undefined') {
			var range = document.selection.createRange();
			var insText = range.text;
			range.text = bbdebut + insText + bbfin;
			range = document.selection.createRange();
			if (insText.length == 0) {
				range.move('character', -bbfin.length);
			}
			else {
				range.moveStart('character', bbdebut.length + insText.length + bbfin.length);
			}
			range.select();
		}
		else if(typeof input.selectionStart != 'undefined') {
			var start = input.selectionStart;
			var end = input.selectionEnd;
			var insText = input.value.substring(start, end);
			input.value = input.value.substr(0, start) + bbdebut + insText + bbfin + input.value.substr(end);
			var pos;
			if (insText.length == 0) {
				pos = start + bbdebut.length;
			}
			else {
				pos = start + bbdebut.length + insText.length + bbfin.length;
			}
			input.selectionStart = pos;
			input.selectionEnd = pos;
		}
		else {
			var pos;
			var re = new RegExp('^[0-9]{0,3}$');
			while(!re.test(pos))
			{
				pos = prompt("insertion (0.." + input.value.length + "):", "0");
			}
			if(pos > input.value.length) {
				pos = input.value.length;
			}
			var insText = prompt("Veuillez taper le texte");
			input.value = input.value.substr(0, pos) + bbdebut + insText + bbfin + input.value.substr(pos);
		}
	}
	function smilies(img) {
		window.document.form.contenu.value += '' + img + '';
	}
	</script>
	<?php
		}
	?>
	<title>Minetest-fr</title>
</head>
<body>
	<div id="content">
		<div id="divlogo">
			<img src="img/minetest-fr.png" alt="minetest-fr" id="logo" />
		</div>
		<div class="navbar">
			<ul>
				<?php
					include 'includes/nav.php';
				?>
			</ul>
		</div>