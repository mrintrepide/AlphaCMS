<?php
function code($texte) {
	//Gras
	$texte = preg_replace('`\[g\](.+)\[/g\]`isU', '<strong>$1</strong>', $texte);
	//Italique 
	$texte = preg_replace('`\[i\](.+)\[/i\]`isU', '<em>$1</em>', $texte);
	//Souligné
	$texte = preg_replace('`\[s\](.+)\[/s\]`isU', '<u>$1</u>', $texte);
	//Lien
	$texte = preg_replace('`\[url\](.+)\[/url\]`isU', '<a href="$1">$1</a>', $texte);
	//Image
	$texte = preg_replace('`\[img\](.+)\[/img\]`isU', '<img src="$1" alt="$1" />', $texte);
	//Titre
	$texte = preg_replace('`\[t\](.+)\[/t\]`isU', '<h3>$1</h3>', $texte);
	//Paragraphe en-tête
	$texte = preg_replace('`\[h\](.+)\[/h\]`isU', '<p>$1</p>', $texte);
	//Paragraphe
	$texte = preg_replace('`\[p\](.+)\[/p\]`isU', '<p>$1</p>', $texte);
	//Liste ul
	$texte = preg_replace('`\[list\](.+)\[/list\]`isU', '<ul>$1</ul>', $texte);
	//Liste li
	$texte = preg_replace('`\[\*\](.+)\[/\*\]`isU', '<li>$1</li>', $texte);
	//Citation
	$texte = preg_replace('`\[quote\](.+)\[/quote\]`isU', '<div class="quote">$1</div>', $texte);
	//Code
	$texte = preg_replace('`\[code\](.+)\[/code\]`isU', '<div class="code">$1</div>', $texte);
	return $texte;
}
?>
