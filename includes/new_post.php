<?php
session_start();
if (isset($_SESSION['id']) AND isset($_SESSION['pseudo']) AND $_SESSION['level']>='1' AND $_GET['sid']==$_SESSION['sid']) {
include('../config.php');
date_default_timezone_set('Europe/Paris');
$date = date('y\-m\-d H:i:s');
$bdd->query('SET NAMES UTF8');
$req = $bdd->prepare('INSERT INTO newsminetest (titre, contenu, signature, date_creation) VALUES(:titre, :contenu, :signature, :date)');
$req->bindValue(':titre', $_POST['titre'], PDO::PARAM_STR);
$req->bindValue(':contenu', $_POST['contenu'], PDO::PARAM_STR);
$req->bindValue(':signature', $_SESSION['pseudo'], PDO::PARAM_STR);
$req->bindValue(':date', $date, PDO::PARAM_STR);
$req->execute();
$req->CloseCursor();
}
header('Location:../index.php');
?>
