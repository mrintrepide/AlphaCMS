<div class="center">
	<h2>Téléchargement</h2>
	<p>Minetest est développer par Perttu Ahola "celeron55" et d'autres contributeurs. Je ne participe pas au développement du jeu.</p>
	<div class="download-integer">
		<h3>Windows (XP/Vista/7/8 32 et 64 bit)</h3>
		<table style="border-spacing: 0; margin: 0; padding: 0; border: 0;">
			<tr>
				<td width="50%" valign="top" style="padding-right: 1em;">
					<ul>
						<li>
							<a href="https://github.com/downloads/celeron55/minetest/minetest-0.4.1-win32.zip">
								<big>Minetest 0.4.1</big> (+ complet, plugins)</a>
						</li>
						<li>
							<a href="https://github.com/downloads/celeron55/minetest/minetest-0.3.1-win32.zip">
								<big>Minetest 0.3.1</big> (rapide, stable et léger)</a>
						</li>
					</ul>
					<ul>
						<li>
							<a href="http://minetest.net/packages/nightly/?C=M;O=A">Version de développement - 
								instable (mis à jour régulièrement)</a>
						</li>
						<li>
							<a href="http://minetest.net/packages/win32">Autres versions</a>
						</li>
						<li>
							<a href="http://c55.me/minetest/oldsite/packages/win32">Anciennes versions</a>
						</li>
					</ul>
				</td>
				<td>
					<p>Décompressez l'archive zip ou vous voulez. Exécuté le programme qui se trouve dans le dossier bin.
						Vous pouvez aussi regarder le wiki 
						<a href="http://wiki.minetest-fr.tk/doku.php?id=comment_bien_debuter">Comment commencer</a>.
					</p>
					<p>Si vous avez un problème regardez <a href="#">ici</a>.</p>
				</td>
			</tr>
		</table>
	</div>
	<div class="download-integer">
		<h3>Mac OS X (portage)</h3>
		<table style="border-spacing: 0; margin: 0; padding: 0; border: 0;">
			<tr>
				<td width="50%" valign="top" style="padding-right: 2em;">
					<ul>
						<li>
							<a href="https://github.com/toabi/minetest-mac/downloads">https://github.com/toabi/minetest-mac/downloads</a>
						</li>
					</ul>
				</td>
				<td>
					<p>(Non supporté par minetest.net)</p>
				</td>
			</tr>
		</table>
	</div>
	<div class="download-integer">
		<h3>Linux (i386 et amd64)</h3>
		<table style="border-spacing: 0; margin: 0; padding: 0; border: 0;">
			<tr>
				<td width="50%" valign="top">
					<h5>Stable (0.3)</h5>
					<ul>
						<li>Debian: inclu dans les dépôts.</li>
						<li>
							<a href="https://community.dev.fedoraproject.org/packages/minetest">Fedora: Inclut dans les dépôts.</a>
						</li>
						<li>
							<a href="http://aur.archlinux.org/packages.php?ID=51142">Arch Linux</a>
						</li>
						<li>
							<a href="https://code.launchpad.net/~minetestdevs/+archive/stable/+packages">Ubuntu/Linux Mint</a>
						</li>
						<li>
							<a href="http://koti.mbnet.fi/juhani4/minetest.php">Puppy Linux</a>
						</li>
					</ul>
					<h5>Développement (0.4-dev)</h5>
					<ul>
						<li>
							<a href="http://aur.archlinux.org/packages.php?ID=45253">Arch Linux</a>
						</li>
						<li>
							<a href="https://code.launchpad.net/~minetestdevs/+archive/daily-builds">Ubuntu/Linux Mint</a>
						</li>
					</ul>
				</td>
				<td valign="top">
					<p>Ces paquets n'ont pas été nécessairement par un développeur de minetest. Contacter l'auteur du paquet en premier.</p>
					<p>Si vous utilisé la version en développement, pensez a sauvgarder votre monde !</p>
					<p>Si votre distribution linux n'est pas répertorié, 
						vous pouvez compiler vous-même minetest grâce au code-source sur Github !</p>
					<p>Si vous avez des portage sur d'autre distribution contacté moi.</p>
				</td>
			</tr>
		</table>
	</div>
	<div>
		<h3>Autres systèmes</h3>
		<p>Il existe un portage pour <a href="http://www.freshports.org/games/minetest/">FreeBSD</a>. Il n'est pas forcément à jour.</p>
		<p>Il est facile de compiler sur OpenBSD. Il doit bien marcher comme toutes les platformes ou Irrlicht est compatible.</p>
	</div>
	<div class="download-integer">
		<h3>Code source</h3>
		<p>
			<a href="https://github.com/celeron55/minetest">Github (dépôt principale)</a>
		</p>
		<h4>Dernière version stable</h4>
		<p>Vous devez avoir 'minetest' et 'minetest_game'.</p>
		<p>'minetest_game' va dans le répertoire 'games/' de minetest.</p>
		<ul>
			<li>
				minetest:
				<a href="https://github.com/celeron55/minetest/tarball/master">tar.gz</a>
				<a href="https://github.com/celeron55/minetest/zipball/master">zip</a>
			</li>
			<li>
				minetest_game:
				<a href="https://github.com/celeron55/minetest_game/tarball/master">tar.gz</a>
				<a href="https://github.com/celeron55/minetest_game/zipball/master">zip</a>
			</li>
		</ul>
		<h4>Ancienne version</h4>
		<ul>
			<li>Stable (très vieux)
				<a href="https://github.com/celeron55/minetest/tarball/stable">tar</a>
				<a href="https://github.com/celeron55/minetest/zipball/stable">zip</a>
			</li>
			<li>0.3.1 (très vieux)
				<a href="https://github.com/celeron55/minetest/tarball/0.3.1">tar</a>
				<a href="https://github.com/celeron55/minetest/zipball/0.3.1">zip</a>
			</li>
			<li>0.3.0 (très vieux)
				<a href="https://github.com/celeron55/minetest/tarball/0.3.0">tar</a>
				<a href="https://github.com/celeron55/minetest/zipball/0.3.0">zip</a>
			</li>
			<li>0.2.20110922_3 (très très vieux)
				<a href="https://github.com/celeron55/minetest/tarball/0.2.20110922_3">tar</a>
				<a href="https://github.com/celeron55/minetest/zipball/0.2.20110922_3">zip</a>
			</li>
		</ul>
	</div>
	<h2>Information sur la compilation</h2>
	<p>
		Lire le fichier REDME.txt : 
		<a href="https://github.com/celeron55/minetest/blob/master/README.txt">(README.txt de la dernière version)</a>
	</p>
	<p>
		Vous pouvez aussi regarder ici 
		<a href="https://github.com/celeron55/minetest/blob/master/util/buildbot/buildwin32.sh">
			pour un script automatique cross-mingw</a>.
	</p>
</div>
