<?php
	include('code.php');
	include('config.php');
	$bdd->query('SET NAMES UTF8');
	$req = $bdd->query('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y\') AS date_creation_fr FROM newsminetest ORDER BY date_creation DESC LIMIT 0, 10');
	while ($donnees = $req->fetch()) {
?>
<div class="news_tab">
<?php
		echo '<h2>
	<a href="index.php?p=post&n='.htmlspecialchars($donnees['id']).'">'.htmlspecialchars($donnees['titre']).'</a>
	</h2>';
		$espace=strrpos($donnees['contenu'],"[/h]"); 
		if($espace!='') {
			echo code(htmlspecialchars(substr($donnees['contenu'],0,$espace+4)));
		}
		else {
			echo code(htmlspecialchars($donnees['contenu']));
		}
?>
</div>
<?php
	}
	$req->closeCursor();
?>
