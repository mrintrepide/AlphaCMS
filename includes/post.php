<?php
	if (isset($_SESSION['id']) AND isset($_SESSION['pseudo'])) {
?>
<form id=form name=form action="includes/new_post.php?sid=<?php echo $_SESSION['sid'] ?>" method="post">
	<table>
		<tr>
			<td>
				<div class="bbcode">
					<input type="image" src="img/bb/bold.png" id="gras" alt="gras" onClick="javascript:bbcode('[g]', '[/g]');return(false)" />
					<input type="image" src="img/bb/italic.png" id="italique" alt="italique" onClick="javascript:bbcode('[i]', '[/i]');return(false)" />
					<input type="image" src="img/bb/underline.png" id="souligné" alt="souligné" onClick="javascript:bbcode('[s]', '[/s]');return(false)" />
					<input type="image" src="img/bb/link.png" id="lien" alt="lien" onClick="javascript:bbcode('[url=]', '[/url]');return(false)" />
					<input type="image" src="img/bb/image.png" id="image" alt="image" onClick="javascript:bbcode('[img]', '[/img]');return(false)" />
					<input type="image" src="img/bb/heading.png" id="titre" alt="titre" onClick="javascript:bbcode('[t]', '[/t]');return(false)" />
					<input type="image" src="img/bb/div.png" id="paragraphe" alt="paragraphe" onClick="javascript:bbcode('[p]', '[/p]');return(false)" />
					<input type="image" src="img/bb/list-unordered.png" id="list" alt="list" onClick="javascript:bbcode('[list]', '[/list]');return(false)" />
					<input type="image" src="img/bb/point.png" id="point" alt="point" onClick="javascript:bbcode('[*]', '[/*]');return(false)" />
					<input type="image" src="img/bb/quote.png" id="quote" alt="quote" onClick="javascript:bbcode('[quote]', '[/quote]');return(false)" />
					<input type="image" src="img/bb/code.png" id="code" alt="code" onClick="javascript:bbcode('[code]', '[/code]');return(false)" />
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<p>Titre : </p>
				<input type="text" size="100" id="titre" name="titre" />
			</td>
		</tr>
		<tr>
			<td>
				<textarea rows="30" cols="98" id="contenu" name="contenu"></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="Poster" />
			</td>
		</tr>
	</table>
</form>
<?php
	}
?>