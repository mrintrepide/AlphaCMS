<div class="center">
<?php
if (isset($_SESSION['id']) AND isset($_SESSION['pseudo'])) {
	include('includes/nav_opt.php');
	if($_GET['r']=='') {
		echo $_SESSION['pseudo'].':'.$_SESSION['sid'];
	}
	elseif($_GET['r']=='post') {
		include('includes/post.php');
	}
	elseif($_GET['r']=='logout') {
		include('includes/logout.php');
	}
	elseif($_GET['r']=='upload') {
		include('includes/img.php');
	}
	else {
		include('error.php');
	}
}
else {
	echo "Vous n'êtes pas connecté !";
}
?>
</div>
