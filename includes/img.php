<?php

// Constantes
define('TARGET', 'files/');		// Repertoire cible
define('MAX_SIZE', 100000);		// Taille max en octets du fichier
define('WIDTH_MAX', 800);		// Largeur max de l'image en pixels
define('HEIGHT_MAX', 800);		// Hauteur max de l'image en pixels

// Tableaux de donnees
$tabExt = array('jpg','gif','png','jpeg');    // Extensions autorisees
$infosImg = array();

// Variables
$extension = null;
$message = null;

/************************************************************
	Creation du repertoire cible si inexistant
*************************************************************/
if( !is_dir(TARGET) ) {
	if( !mkdir(TARGET, 0700) ) {
		exit('Erreur : le répertoire cible ne peut-être créé ! Vérifiez que vous diposiez des droits suffisants pour le faire ou créez le manuellement !');
	}
}

/************************************************************
	Script d'upload
*************************************************************/
if($_POST)
{
  // On verifie si le champ est rempli
  if( !empty($_FILES['fichier']['name']) )
  {
	// Recuperation de l'extension du fichier
	$extension	= pathinfo($_FILES['fichier']['name'], PATHINFO_EXTENSION);
	
    // On verifie l'extension du fichier
    if(in_array(strtolower($extension),$tabExt))
    {
        // On recupere les dimensions du fichier
        $infosImg = getimagesize($_FILES['fichier']['tmp_name']);
        
        // On verifie le type de l'image
        if($infosImg[2] >= 1 && $infosImg[2] <= 14)
        {
			// On verifie les dimensions et taille de l'image
			if(($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($_FILES['fichier']['tmp_name']) <= MAX_SIZE))
			{
				// Si c'est OK, on teste l'upload
				if(move_uploaded_file($_FILES['fichier']['tmp_name'], TARGET.$_FILES['fichier']['name']))
				{
					// Message de reussite
					$message = 'Upload réussi !';
				}
				   else
				{
					// Sinon on affiche une erreur systeme
					$message = 'Problème lors de l\'upload !';
				}
			}
			  else
			{
				// Sinon erreur sur les dimensions et taille de l'image
				$message = 'Erreur dans les dimensions de l\'image !';
			}
        }
          else
        {
            // Sinon erreur sur les dimensions et taille de l'image
            $message = 'Le fichier à uploader n\'est pas une image !';
        }
    }
      else
    {
        // Sinon on affiche une erreur pour l'extension
        $message = 'L\'extension du fichier est incorrecte !';
    }
   }
    else
   {
        // Sinon on affiche une erreur pour le champ vide
        $message = 'Veuillez remplir le formulaire svp !';
   }
}

/************************************************************
	Formulaire XHTML
*************************************************************/

if( !empty($message) ) {
	echo '<p>',"\n";
	echo "\t\t<strong>", htmlspecialchars($message) ,"</strong>\n";
	echo '<a href="http://img.minetest-fr.tk/'.$_FILES['fichier']['name'].'">http://img.minetest-fr.tk/'.$_FILES['fichier']['name'].'</a>';
	echo "\t</p>\n\n";
}
?>

<form enctype="multipart/form-data" action="index.php?p=option&r=upload" method="post">
	<fieldset>
		<legend>Formulaire</legend>
		<p>
			<label for="fichier_a_uploader" title="Recherchez le fichier à uploader !">Envoyer le fichier :</label>
			<input type="hidden" name="posted" value="1" />
			<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo MAX_SIZE; ?>" />
			<input name="fichier" type="file" id="fichier_a_uploader" />
			<input type="submit" name="submit" value="Uploader" />
		</p>
	</fieldset>
</form>