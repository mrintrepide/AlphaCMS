<table id="ville">
	<tr>
		<th>Nom de la ville</th>
		<th>Nom du maire (responsable)</th>
		<th colspan="4">Drapeau (téléportation)</th>
	</tr>
	<tr>
		<td>Steinheim</td>
		<td>prof-chocolate</td>
		<td style="background:rgb(255,255,51);width:60px;"></td>
		<td style="background:rgb(51,51,255);width:60px;"></td>
		<td style="background:rgb(51,51,255);width:60px;"></td>
		<td style="background:rgb(255,255,51);width:60px;"></td>
	</tr>
	<tr>
		<td>MineHill</td>
		<td>Roddy</td>
		<td style="background:rgb(51,51,255);width:60px;"></td>
		<td style="background:rgb(102,204,204);width:60px;"></td>
		<td style="background:rgb(102,204,204);width:60px;"></td>
		<td style="background:rgb(51,51,255);width:60px;"></td>
	</tr>
	<tr>
		<td>Chateau fort d'émeraude</td>
		<td>Mr Green</td>
		<td style="background:rgb(153,51,153);width:60px;"></td>
		<td style="background:rgb(255,255,51);width:60px;"></td>
		<td style="background:rgb(51,51,255);width:60px;"></td>
		<td style="background:rgb(255,255,255);width:60px;"></td>
	</tr>
	<tr>
		<td>MineStein</td>
		<td>Jat</td>
		<td style="background:rgb(153,51,153);width:60px;"></td>
		<td style="background:rgb(255,153,0);width:60px;"></td>
		<td style="background:rgb(255,153,0);width:60px;"></td>
		<td style="background:rgb(153,51,153);width:60px;"></td>
	</tr>
	<tr>
		<td>AnimaStadt</td>
		<td>prof-turbo</td>
		<td style="background:rgb(255,255,51);width:60px;"></td>
		<td style="background:rgb(102,102,102);width:60px;"></td>
		<td style="background:rgb(102,102,102);width:60px;"></td>
		<td style="background:rgb(255,255,51);width:60px;"></td>
	</tr>
	<tr>
		<td>Village du desert</td>
		<td>hmsintrepide</td>
		<td style="background:rgb(255,255,51);width:60px;"></td>
		<td style="background:rgb(0,153,0);width:60px;"></td>
		<td style="background:rgb(0,0,0);width:60px;"></td>
		<td style="background:rgb(51,51,255);width:60px;"></td>
	</tr>
	<tr>
		<td>Arbre Laori</td>
		<td>Daemon</td>
		<td style="background:rgb(0,153,0);width:60px;"></td>
		<td style="background:rgb(255,153,0);width:60px;"></td>
		<td style="background:rgb(255,153,0);width:60px;"></td>
		<td style="background:rgb(0,153,0);width:60px;"></td>
	</tr>
</table>